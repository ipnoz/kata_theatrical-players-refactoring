public class PlayFactory {

    public static Play create(String type, String id, String name) {
        switch (type) {
            case "tragedy":
                return new TragedyPlay(id, name);
            case "comedy":
                return new ComedyPlay(id, name);
            default:
                throw new Error("unknown type: ${type}");
        }
    }
}
