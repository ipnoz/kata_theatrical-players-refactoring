public class Performance {

    public Play play;
    public int audience;
    public int amount;
    public int credits;

    public Performance(Play play, int audience) {
        this.play = play;
        this.audience = audience;
        this.amount = calculateAmount();
        this.credits = calculateCredits();
    }

    private int calculateAmount() {
        return (this.play.getAmount(this.audience) / 100);
    }

    private int calculateCredits() {

        int credits = Math.max(this.audience - 30, 0);
        credits += this.play.getExtraCredits(this.audience);

        return credits;
    }
}
