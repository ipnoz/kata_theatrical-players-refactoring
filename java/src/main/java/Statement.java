import java.util.List;

public class Statement {

    public Invoice invoice;

    public String customer;
    public List<Performance> performances;

    public int totalOfAmount = 0;
    public int totalOfCredits = 0;

    public Statement(Invoice invoice) {

        this.invoice = invoice;

        this.customer = invoice.customer;
        this.performances = invoice.performances;

        for (Performance performance : this.performances) {
            this.totalOfAmount += performance.amount;
            this.totalOfCredits += performance.credits;
        }
    }
}
