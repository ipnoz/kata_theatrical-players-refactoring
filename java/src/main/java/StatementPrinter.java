import java.text.NumberFormat;
import java.util.Locale;

public class StatementPrinter {

    public String print(Invoice invoice) {

        Statement statement = new Statement(invoice);

        var result = String.format("Statement for %s\n", statement.customer);

        for (Performance performance : statement.performances) {

            result += String.format(
                    "  %s: %s (%s seats)\n",
                    performance.play.name,
                    usd(performance.amount),
                    performance.audience
            );
        }

        result += String.format("Amount owed is %s\n", usd(statement.totalOfAmount));
        result += String.format("You earned %s credits\n", statement.totalOfCredits);

        return result;
    }

    private String usd(int amount) {
        return NumberFormat.getCurrencyInstance(Locale.US).format(amount);
    }
}
