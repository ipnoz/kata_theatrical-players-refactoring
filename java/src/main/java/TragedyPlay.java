public class TragedyPlay extends Play {

    public TragedyPlay(String id, String name) {
        super(id, name);
    }

    public String type() {
        return "tragedy";
    }

    @Override
    public int getAmount(int audience) {

        int amount = 40000;

        if (audience > 30) {
            amount += 1000 * (audience - 30);
        }

        return amount;
    }
}
