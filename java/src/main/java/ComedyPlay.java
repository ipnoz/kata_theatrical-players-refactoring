public class ComedyPlay extends Play {

    public ComedyPlay(String id, String name) {
        super(id, name);
    }

    public String type() {
        return "comedy";
    }

    @Override
    public int getAmount(int audience) {

        int amount = 30000;

        if (audience > 20) {
            amount += 10000 + 500 * (audience - 20);
        }

        amount += 300 * audience;

        return amount;
    }

    @Override
    // add extra credit for every ten comedy attendees
    // @fixme: we expect extra credits for 10 attendees, but the function divide by 5
    public double getExtraCredits(double audience) {
        return Math.floor(audience / 5);
    }
}
