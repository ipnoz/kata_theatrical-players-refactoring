abstract class Play {

    public String id;
    public String name;

    public Play(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String type() {
        return "Play";
    }

    public int getAmount(int audience) {
        return 0;
    }

    public double getExtraCredits(double audience) {
        return 0;
    }
}
