import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.approvaltests.Approvals.verify;

public class StatementPrinterTests {

    @Test
    void PlayWithNewTypes() {
        Assertions.assertThrows(Error.class, () -> {
            PlayFactory.create("history", "henry-v", "Henry V");
        });
    }

    @Test
    void exampleStatement() {
        Invoice invoice = new Invoice("BigCo", List.of(
            new Performance(PlayFactory.create("tragedy", "hamlet", "Hamlet"), 55),
            new Performance(PlayFactory.create("comedy", "as-like", "As You Like It"), 35),
            new Performance(PlayFactory.create("tragedy", "othello", "Othello"), 40)));

        StatementPrinter statementPrinter = new StatementPrinter();
        var result = statementPrinter.print(invoice);

        verify(result);
    }
}
